/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figuras1;

/**
 *
 * @author milton
 */
public abstract class FormasTrigonometricas{

    private int numLados;

    public FormasTrigonometricas() {

    }

    public FormasTrigonometricas(int numLado) {
        this.numLados = numLado;
    }

    public int getNumLados() {
        return numLados;
    }

    public void setNumLados(int numLados) {
        this.numLados = numLados;
    }

    @Override
    public String toString() {
        return " Area  : " +  numLados ;
    }

    public abstract double area();
    public abstract double perimetro();
}
