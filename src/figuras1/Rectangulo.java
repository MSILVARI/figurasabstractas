/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figuras1;

/**
 *
 * @author sigmotoa
 */
public class Rectangulo extends FormasTrigonometricas {

    private double base;
    private double altura;

    public Rectangulo(double base, double altura) {
        super(2);
        this.base = base;
        this.altura = altura;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }
    
    @Override
    public String toString() {
        return "Rectangulo " + super.toString()
                + "\nBase  = " + base + " , Altura  = " + altura;
    }

    @Override
    public double area() {
        return base * altura;
    }

    @Override
    public double perimetro() {
        return (base * 2) + (altura + 2);
    }
}
