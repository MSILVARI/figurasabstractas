/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formastrigonometricas;

import figuras1.Circulo;
import figuras1.FormasTrigonometricas;
import figuras1.Rectangulo;
import figuras1.Triangulo;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author milton
 */
public class TesFiguras {

    static Scanner sc = new Scanner(System.in);

    static ArrayList<FormasTrigonometricas> formasTrigonometricas = new ArrayList<FormasTrigonometricas>();

    public static void main(String[] args) {

        LeerFormas();
        mostrarPoligonos();
        
    }

    public static void LeerFormas() {
        int tipo;
        do {
            do {
                System.out.print("Tipo de Forma 1-> Rectangulo 2-> Triangulo 3-> Circulo 0-> FIN y Mostar Resultado >>> ");
                tipo = sc.nextInt();
            } while (tipo < 0 || tipo > 3);
            if (tipo != 0) {
                switch (tipo) {
                    case 1:
                        leerRectangulo();
                        break;
                    case 2:
                        leerTriangulo();
                    case 3:
                        leerCirculo();
                        break;
                }
            }
        } while (tipo != 0);
    }

    public static void leerRectangulo() {
        double l1, l2;
        System.out.println("Introduzca datos del Rectángulo");
        do {
            System.out.print("Longitud del lado 1: ");
            l1 = sc.nextDouble();
        } while (l1 <= 0);
        do {
            System.out.print("Longitud del lado 2: ");
            l2 = sc.nextDouble();
        } while (l2 <= 0);
        Rectangulo r = new Rectangulo(l1, l2);
        formasTrigonometricas.add(r);

    }

    static void leerTriangulo() {
        double l1, l2, l3;
        System.out.println("Introduzca datos del Triangulo");
        do {
            System.out.print("Longitud del lado 1: ");
            l1 = sc.nextDouble();
        } while (l1 <= 0);
        do {
            System.out.print("Longitud del lado 2: ");
            l2 = sc.nextDouble();
        } while (l2 <= 0);
        do {
            System.out.print("Longitud del lado 3: ");
            l3 = sc.nextDouble();
        } while (l3 <= 0);
        Triangulo t = new Triangulo(l1, l2, l3);
        formasTrigonometricas.add(t);

    }
    static void leerCirculo() {
        double r1;
        System.out.println("Introduzca los datos del Circulo");
        do {
            System.out.print("Introduca el radio1: ");
            r1 = sc.nextDouble();
        } while (r1 <= 0);
     
        Circulo c1 = new Circulo(r1);
        formasTrigonometricas.add(c1);

    }

    public static void mostrarPoligonos() {

        for (FormasTrigonometricas p : formasTrigonometricas) {
            System.out.print(p.toString());
            System.out.printf(" area: %.2f %n", p.area());
            System.out.printf(" area: %.2f %n", p.perimetro());
         
        }
    }
}
